#!/system/bin/sh
FILE_USB_MODE="/sys/bus/platform/drivers/usb20_otg/force_usb_mode"
MODE=$(getprop persist.usb.mode)

if [ -n "$MODE" ];then
     [ -e "$FILE_USB_MODE" ] &&  echo  "$MODE" > "$FILE_USB_MODE"
fi

