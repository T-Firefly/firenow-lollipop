#!/bin/bash
set -e


. build/envsetup.sh >/dev/null

TARGET_PRODUCT=`get_build_var TARGET_PRODUCT`
IMAGE_SRC_CUR_PATH=rockdev/Image-$TARGET_PRODUCT

if [ $TARGET_PRODUCT = "aio_3128c" ] ; then
    OPT_CODENAME="AIO-3128C"
else
    OPT_CODENAME="Fireprime"
fi



pushd $IMAGE_SRC_CUR_PATH
for IMG in ${OPT_CODENAME}*.img; do
    IMG_7Z=${IMG%.*}.7z
    if [ -f ${IMG_7Z} ] && [ -f ${IMG_7Z}.md5sum ];
    then
        continue
    else
        rm -f $IMG_7Z
    fi

    if [ ! -f ${IMG} ] ; then
        echo "Not IMG packing"
        break
    fi

    echo Compressing $IMG...
    7zr a -t7z -m0=lzma2 -mx=9 -mfb=64 -md=64m -ms=on -mmt=on $IMG_7Z $IMG \
        && ls -lh $IMG_7Z \
        && md5sum $IMG_7Z >$IMG_7Z.md5sum

    if [ -d ~/vmware ];
    then
        cp $IMG_7Z ~/vmware
        echo "cp vmware dir"
    fi
done
