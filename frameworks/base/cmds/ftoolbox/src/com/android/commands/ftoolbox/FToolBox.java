package com.android.commands.ftoolbox;




import android.util.Log;
import android.os.RemoteException;
import android.os.ServiceManager;

import android.os.Environment;

import android.net.IpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.net.StaticIpConfiguration;
import android.text.TextUtils;
import java.io.File;
import java.io.IOException;
import android.content.Context;

import android.firefly.util.NetworkUtils;

import android.os.storage.IMountService;
import android.os.ServiceManager;
import android.os.IFireflyManager;
import android.net.IEthernetManager;
import android.net.EthernetManager;
import android.view.Surface;

import android.net.IConnectivityManager;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;


import android.os.SystemProperties;
import android.net.LinkAddress;
import java.util.ArrayList;
import java.net.InetAddress;

import android.hardware.display.IDisplayManager;
import android.view.Display;

import android.firefly.util.FileUtils;

import android.app.IAlarmManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;

import android.os.PowerManager;
import android.os.IPowerManager;
import android.os.RecoverySystem;

public class FToolBox{
    private static final String TAG = "FToolBox";
    private String[] mArgs;
    
    private static final boolean DEBUG = true;
 
    private static final String ERROR = null;
    private static final String UNKNOWN = "unknown";

    IEthernetManager mEthernetManager;
    IMountService mMountService;

    IFireflyManager mFireflyManager;
    IConnectivityManager mConnectivityManager;

    IDisplayManager mDisplayManager;

    IAlarmManager mAlarmManager;

    private static final String FIREFLY_NOT_RUNNING_ERR =
        "Error: Could not access the FireflyAPi.  Is the system running?";


    public static void main(String[] args) {
        int exitCode = 1;
        try {
            exitCode = new FToolBox().run(args);
        } catch (Exception e) {
            Log.e(TAG, "Error", e);
            System.err.println("Error: " + e);
            if (e instanceof RemoteException) {
                System.err.println(FIREFLY_NOT_RUNNING_ERR);
            }
        }
        System.exit(exitCode);
    }

    public int run(String[] args) throws IOException, RemoteException {
        boolean validCommand = false;
        if (args.length < 1) {
            return showUsage();
        }
        mEthernetManager = IEthernetManager.Stub.asInterface(ServiceManager.getService("ethernet"));

        mMountService = IMountService.Stub.asInterface(ServiceManager.getService("mount"));

        mFireflyManager = IFireflyManager.Stub.asInterface(ServiceManager.getService(Context.FIREFLY_SERVICE));

        mConnectivityManager = IConnectivityManager.Stub.asInterface(ServiceManager.getService(Context.CONNECTIVITY_SERVICE));
        
        mDisplayManager = IDisplayManager.Stub.asInterface(ServiceManager.getService(Context.DISPLAY_SERVICE));
        
        mAlarmManager = IAlarmManager.Stub.asInterface(ServiceManager.getService(Context.ALARM_SERVICE));

        mArgs = args;
        String op = args[0];
        // mNextArg = 1;
        // Log.e(TAG, "op:"+op);

        for(int i=0;i<args.length;i++){
            Log.e(TAG, "xxx args["+i+"]:"+args[i]);
            
        }

        if("ethernet".equals(op)){
            return runEthernet();
        }

        if("mountService".equals(op)){
            return runMountService();
        }

        if("recoveryControl".equals(op)){
            return runRecoveryControl();
        }

        if("systemControl".equals(op)){
            return runSystemControl();
        }


        if("hardwareControl".equals(op)){
            return runHardwareControl();
        }

        return 1;
        
    }

    private boolean isEthConnect()throws  RemoteException{
        NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
        if(mNetworkInfo != null && mNetworkInfo.isConnected() && mNetworkInfo.getType() == ConnectivityManager.TYPE_ETHERNET){
            return true;
        }
        
        return false;
    }

    private final static String nullIpInfo = "0.0.0.0";

    private int runEthernet()  throws IOException, RemoteException{
        if(mArgs.length < 2) return 1;
        
        String op = mArgs[1];
        Log.e(TAG, "runEthernet:"+op);

        if ("setIpAddress".equals(op)) {
            if(mArgs.length < 3) return 1;
                
            boolean useStatic = mArgs[2].equals("static") || mArgs[2].equals("true")|| mArgs[2].equals("1");
            if(useStatic)
            {
                if(mArgs.length == 8){
                    //fireflyapi ethernet setIpAddress static/true/1 168.168.100.245 255.255.0.0 168.168.0.1 202.96.128.86 202.96.128.166

                    String ipaddress = mArgs[3];
                    String netmask = mArgs[4];
                    String gateway = mArgs[5];
                    String dns1 = mArgs[6];
                    String dns2 = mArgs[7];
                        // Log.v(TAG,"setEthIPAddress useStatic:"+useStatic+",ipaddress:"+ipaddress+",netmask:"+netmask
                        //     +",gateway:"+gateway+",dns1:"+dns1+",dns2:"+dns2);
                    IpConfiguration mIpConfiguration = NetworkUtils.getStaticIpConfiguration(ipaddress, netmask, gateway, dns1, dns2);

                    int preState=mEthernetManager.getEthernetInterfaceState();
                    mEthernetManager.setEthernetEnabled(false);
                    mEthernetManager.setConfiguration(mIpConfiguration);
                    if (preState==EthernetManager.ETHER_IFACE_STATE_UP) {
                                mEthernetManager.setEthernetEnabled(true);
                    }
                    return 0;
                }else{
                    return 1;
                }
            }else{
                    int preState=mEthernetManager.getEthernetInterfaceState();
                    mEthernetManager.setEthernetEnabled(false);
                    mEthernetManager.setConfiguration(new IpConfiguration(IpAssignment.DHCP, ProxySettings.NONE,null,null));
                    if (preState==EthernetManager.ETHER_IFACE_STATE_UP) {
                                mEthernetManager.setEthernetEnabled(true);
                    }
                return 0;
            }
 
        }

        if("getIpAddress".equals(op)){
            String ipAddress = null;
            IpAssignment mode =mEthernetManager.getConfiguration().getIpAssignment();
            if(isEthConnect())
            {
                if ( mode== IpAssignment.DHCP) {
                    String iface = "eth0";
                    ipAddress = SystemProperties.get("dhcp."+ iface +".ipaddress");
                } else if(mode == IpAssignment.STATIC) {
                    StaticIpConfiguration staticIpConfiguration=mEthernetManager.getConfiguration().getStaticIpConfiguration();
                    if(staticIpConfiguration != null) {
                        LinkAddress mLinkAddress = staticIpConfiguration.ipAddress;
                        if(mLinkAddress != null){
                            ipAddress = mLinkAddress.getAddress().getHostAddress();
                        }
                    }
                }
            }

            if(TextUtils.isEmpty(ipAddress))
            {
                System.out.print(ERROR);
                return 1;
            }else{
                System.out.print(ipAddress);
                return 0;
            } 
        }

        if("getEthInfo".equals(op)){
            boolean useStatic = false;
            String ipaddress = null;
            String netmask = null;
            String gateway = null;
            String dns1 = null;
            String dns2 = null;
            IpAssignment mode =mEthernetManager.getConfiguration().getIpAssignment();
            if(isEthConnect())
            {
                if ( mode== IpAssignment.DHCP) {
                    useStatic = false;
                    String iface = "eth0";
                
                    String tempIpInfo = SystemProperties.get("dhcp."+ iface +".ipaddress");
                
                    if(!TextUtils.isEmpty(tempIpInfo)){
                        ipaddress = tempIpInfo;
                    }
                
                    tempIpInfo = SystemProperties.get("dhcp."+ iface +".mask");
                    if(!TextUtils.isEmpty(tempIpInfo)){
                        netmask = tempIpInfo;
                    }

                    tempIpInfo = SystemProperties.get("dhcp."+ iface +".gateway");  
                    if(!TextUtils.isEmpty(tempIpInfo)){
                        gateway = tempIpInfo;
                    }
                
                    tempIpInfo = SystemProperties.get("dhcp."+ iface +".dns1");
                    if(!TextUtils.isEmpty(tempIpInfo)){
                        dns1 = tempIpInfo;
                    }

                    tempIpInfo = SystemProperties.get("dhcp."+ iface +".dns2");
                    if(!TextUtils.isEmpty(tempIpInfo)){
                        dns2= tempIpInfo;
                    }

                } else if(mode == IpAssignment.STATIC) {
                    useStatic = true;
                    StaticIpConfiguration staticIpConfiguration=mEthernetManager.getConfiguration().getStaticIpConfiguration();
                    if(staticIpConfiguration != null) {
                        LinkAddress mLinkAddress = staticIpConfiguration.ipAddress;
                        if(mLinkAddress != null){
                            LinkAddress mIpAddress = staticIpConfiguration.ipAddress;
                            InetAddress mGateWay   = staticIpConfiguration.gateway;
                            ArrayList<InetAddress> dnsServers=staticIpConfiguration.dnsServers;
                            if( mIpAddress !=null) {
                                ipaddress = mIpAddress.getAddress().getHostAddress();
                                netmask = NetworkUtils.interMask2String(mIpAddress.getPrefixLength());
                            }
                            if(mGateWay !=null) {
                                gateway = mGateWay.getHostAddress();
                            }
                            dns1 = dnsServers.get(0).getHostAddress();                       
                            if(dnsServers.size() > 1) { /* 只保留两个*/
                                dns2 = dnsServers.get(1).getHostAddress();          
                            }       
                        }
                    }
                }

                if(!TextUtils.isEmpty(ipaddress)){
                    System.out.print(useStatic);
                    System.out.print(ipaddress);
                    System.out.print(netmask);
                    System.out.print(gateway);
                    System.out.print(dns1);
                    System.out.print(dns2);
                    return 0;
                }else{
                    System.out.print(ERROR);
                    return 1;
                }
            }
        }

        if("setEnabled".equals(op)){
            if(mArgs.length == 3){
                boolean enabled = !mArgs[2].equals("0");
                mEthernetManager.setEthernetEnabled(enabled);
                //Log.v(TAG,"mEthM.setEthernetEnabled(enabled):"+success);
                //System.err.println(success?0:1);
                return 0;
            }else{
               return 1; 
            }
        }


        if("getConnectState".equals(op)){
            int state = mEthernetManager.getEthernetConnectState();
            //Log.v(TAG,"getEthernetConnectState:"+state);
            System.out.print(state);
            return 0; 
        }

        if("isConnect".equals(op)){
            System.out.print(isEthConnect()?"1":"0");
            return 0; 
        }

        if("getCurNetworkType".equals(op)){
            String strNetworkType = "UNKNOWN";
            NetworkInfo networkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected())
            {
                if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI)
                {
                    strNetworkType = "WIFI";
                }else if (networkInfo.getType() == ConnectivityManager.TYPE_ETHERNET)
                {
                    strNetworkType = "ETHERNET";
                }
                else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE)
                {
                    strNetworkType = "MOBILE";

                }
            }
            System.out.print(strNetworkType);
            return 0; 
        }


        return 1;
    }


    private int runMountService()  throws IOException, RemoteException{
        if(mArgs.length < 2) return 1;
        
        String op = mArgs[1];

        if("doUnmountVolume".equals(op)){
            //doUnmountVolume(String path,boolean force,boolean removeEncryption)
            //fireflyapi mountService doUnmountVolume /mnt/usb_storage/USB_DISK2 1 0
            if(mArgs.length == 5){
                String path = mArgs[2];
                boolean force = !mArgs[3].equals("0");
                boolean removeEncryption = !mArgs[4].equals("0");
                //Log.v(TAG,"doUnmountVolume path:"+path+",force:"+force+",removeEncryption:"+removeEncryption);
                try {
                    mMountService.unmountVolume(path,force,removeEncryption);
                    System.out.print(0);
                } catch (RemoteException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    System.out.print(1);
                }

                return 0;
            }else{
               return 1; 
            }
        }

        return 1;
    }
    private int runRecoveryControl()  throws IOException, RemoteException{
        if(mArgs.length < 2) return 1;
        
        String op = mArgs[1];
        if("installPackage".equals(op)){
            //fireflyapi installPackage /mnt/sdcard/update.zip
            if(mArgs.length == 3){
                String path = mArgs[2];
                if(!TextUtils.isEmpty(path)){   
                    path = new File(path).getAbsolutePath();
                    if(path.startsWith("/mnt/sdcard/")){
                        path = path.replace("/mnt/sdcard/", "/data/media/0/");
                    }else if(path.startsWith("/sdcard/")){
                        path = path.replace("/sdcard/", "/data/media/0/");
                    }

                    IPowerManager pm = IPowerManager.Stub.asInterface(
                        ServiceManager.getService(Context.POWER_SERVICE));

                    File packageFile = new File(path);
                    // String filename = packageFile.getCanonicalPath();
                    // RKRecoverySystem.writeFlagCommand(filename);
                    RecoverySystem.installPackageCommand(packageFile);

                    pm.reboot(false,PowerManager.REBOOT_RECOVERY,true);

                    return 0;
                }else{
                    return 1;
                }
            

            }else{
                return 1; 
            }
        }else if("getOtaResult".equals(op)){
            // fireflyapi installPackage /mnt/sdcard/update.zip
            // String command = RKRecoverySystem.readFlagCommand();
            // String path;
            // if(command != null) {
            //     if(command.contains("$path")) {
            //         path = command.substring(command.indexOf('=') + 1);
                
            //         if(command.startsWith(RKRecoverySystem.COMMAND_FLAG_SUCCESS)) {
            //             System.out.print("SUCCESS");
            //         } 
            //         if(command.startsWith(RKRecoverySystem.COMMAND_FLAG_UPDATING)) {
            //             System.out.print("FAILED");
            //         }

            //         System.out.print(path);

            //         return 0;
            //     }
            // }
        }

        return 1;
    }

    private int runSystemControl()  throws IOException, RemoteException{
        if(mArgs.length < 2) return 1;
        
        String op = mArgs[1];
        if("sleep".equals(op)){
           mFireflyManager.sleep();
           return 0;
        }
        
        if("reboot".equals(op)){
           mFireflyManager.reboot(false, null);
           return 0;
        }

        if("shutdown".equals(op)){
            boolean showConfim ;
            if(mArgs.length == 2){
                showConfim = false;
            }else{
              showConfim = !mArgs[2].equals("0");
            }
           mFireflyManager.shutdown(showConfim);
           return 0;
        }

        if("setRotation".equals(op)){
            if(mArgs.length <= 2){
                return 1;
            }
            int rotation = Integer.parseInt(mArgs[2]);
            switch (rotation) {
                // case 0:
                case Surface.ROTATION_0:
                    mFireflyManager.setRotation(Surface.ROTATION_0);
                    return 0;
                case 90:
                case Surface.ROTATION_90:
                    mFireflyManager.setRotation(Surface.ROTATION_90);
                    return 0;
                case 180:
                case Surface.ROTATION_180:
                    mFireflyManager.setRotation(Surface.ROTATION_180);
                    return 0;
                case 270:
                case Surface.ROTATION_270:
                    mFireflyManager.setRotation(Surface.ROTATION_270);
                    return 0;
                default:
                    return 1;
            }
           
        }
 
        if("getRotation".equals(op)){
            
           System.out.print(mFireflyManager.getRotation());
           return 0;
        } 

        if("thawRotation".equals(op)){
            
           mFireflyManager.thawRotation();
           return 0;
        }   

        if("hideStatusBar".equals(op)){
            
           mFireflyManager.setStatusBar(false);
           return 0;
        } 

        if("showStatusBar".equals(op)){
            
           mFireflyManager.setStatusBar(true);
           return 0;
        }

        return 1;
        
    }


    
    private static final String HDMI_STATUS_FILE = "/sys/class/drm/card0-HDMI-A-1/status";


    private int runHardwareControl() throws IOException, RemoteException{
        if(mArgs.length < 2) return 1;
        
        String op = mArgs[1];

        if("getScreenNumber".equals(op)){
           int dispArray[] = mDisplayManager.getDisplayIds();
           if(dispArray == null)
           {
              System.out.print(0);
             return 1;
           }

            System.out.print(dispArray.length);
           return 0;
        }

        if("getHdmiinStatus".equals(op)){
          // String status  =  mFireflyManager.getHdmiinStatus();
          // return status;
             return 1;
        }

        // if("switchMic".equals(op)){
        //     if(mArgs.length <= 2){
        //         return 1;
        //     }
        //     int mic_type = Integer.parseInt(mArgs[2]);
        //     if( mFireflyManager.switchMic(mic_type))
        //     {
        //         return 0;
        //     }
        //     return 1;
        // }


        if("setUsbPower".equals(op)){
            if(mArgs.length <= 3){
                return 1;
            }
            int type = Integer.parseInt(mArgs[2]);
            boolean  connect = "true".equals(mArgs[3])|| "1".equals(mArgs[3]);
            if( mFireflyManager.setUsbPower(type,connect))
            {
                return 0;
            }
            return 1;
        }

        if("watchDogEnable".equals(op)){
            if(mArgs.length <= 2){
                return 1;
            }
            boolean  enable = "true".equals(mArgs[2])|| "1".equals(mArgs[2]);
            if( mFireflyManager.watchDogEnable(enable))
            {
                return 0;
            }
            return 1;
        }

        if("watchDogFeed".equals(op)){
            if( mFireflyManager.watchDogFeed())
            {
                return 0;
            }
            return 1;
        }

        // if("getUSBPath".equals(op)){
        //   if(mArgs.length < 3) return 1;
        //    String path = null;
        //    int num = Integer.parseInt(mArgs[2]);
        //    switch (num) {
        //     case 1:
        //         path = Environment.getHostStorage_Extern_1_Directory().getAbsolutePath();
        //     case 2:
        //         path = Environment.getHostStorage_Extern_2_Directory().getAbsolutePath();
        //     case 3:
        //         path = Environment.getHostStorage_Extern_3_Directory().getAbsolutePath();
        //     case 4:
        //         path = Environment.getHostStorage_Extern_4_Directory().getAbsolutePath();
        //     case 5:
        //         path = Environment.getHostStorage_Extern_5_Directory().getAbsolutePath();

        //     default:
        //         break;
        //     }
        //     if(path != null){
        //         System.err.println(path);
        //         return 0;

        //     }else{
        //         return 1;
        //     }
        
             
        // }
        
        // if("getSDcardPath".equals(op)){
        //   System.err.println(Environment.getSecondVolumeStorageDirectory().getAbsolutePath());
        //   return 0;
             
        // } 

        return 1;
        
    }

    

    private static int showUsage() {
       // System.err.println("usage: fireflyapi setEthIPAddress <useStatic> <ipaddress> <netmask> <gateway> <dns1> <dns2>");
        //System.err.println("       (exapmle:fireflyapi setEthIPAddress 0 168.168.100.245 255.255.0.0 168.168.0.1 202.96.128.86 202.96.128.166)");
        //System.err.println("       fireflyapi setEthernetEnabled [1/enable] [2/disable]");
        //System.err.println("       fireflyapi doUnmountVolume <path> <force(1/0)> <removeEncryption(1/0)>");
        //System.err.println("");
        return 1;
    }


 
}
