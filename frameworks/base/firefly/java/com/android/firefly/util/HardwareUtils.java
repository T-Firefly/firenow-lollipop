package android.firefly.util;

import android.app.FireflyManager;

public class HardwareUtils {

    /***
     * 打开/关闭看门狗
     * @param enable
     */
    public  static String  buildWatchDogEnableCmd(boolean enable){
        if(enable)
        	return "echo  0> /dev/watchdog";
        else
        	return "echo  V> /dev/watchdog";
    }
    /***
     * 喂狗一次，对看门狗计数进行复位操作。
     * @param enable
     */
    public  static String buildWatchDogFeedCmd(){
        return "echo  0> /dev/watchdog";
    }


    /***
     * 设置USB接口电源,选择控制OTG或USB电源接口
     *  echo 1 > /sys/class/otg_hw_ctl/otg_hw_ctl  连接OTG
     *  echo 0 > /sys/class/otg_hw_ctl/otg_hw_ctl  断开OTG
     *  echo 1 > /sys/class/host_hw_ctl/host_hw_ctl  连接USB HOST
     *  echo 0 > /sys/class/host_hw_ctl/host_hw_ctl  断开USB HOST
     * @param type TYPE_USBHOST,TYPE_OTG
     * @param connect 
     * @return
     */

    public  static String buildSetUsbPowerCmd(int type, boolean connect ){
        if(type == FireflyManager.TYPE_USBHOST){
        	if(connect)
        	{
        		return "echo  1 > /sys/class/host_hw_ctl/host_hw_ctl";
        	}else{
        		return "echo  0 > /sys/class/host_hw_ctl/host_hw_ctl";
        	}
        }else if(type == FireflyManager.TYPE_OTG){
        	if(connect)
        	{
        		return "echo  1 >  /sys/class/otg_hw_ctl/otg_hw_ctl";
        	}else{
        		return "echo  0 > /sys/class/otg_hw_ctl/otg_hw_ctl";
        	}
        }
        return null;
    }

}