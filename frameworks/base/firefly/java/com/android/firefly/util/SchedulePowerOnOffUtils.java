package android.firefly.util;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;

public class SchedulePowerOnOffUtils {
	
    	private final static String ACTION_SCHEDULE_POWER_ON = "android.fireflyapi.action.run_power_on";
	/***
	 * 设置定时开机,id由用户定义，用于开启和关闭定时开机时使用，重启后失效需要重新设置
	 * @param id　　　　　　　　　
	 * @param enabled　　　　开启/关闭
	 * @param alarm_time　开机时间(UTC时间)
	 */
	public static void setSchedulePowerOn(Context context ,int id,boolean enabled,long alarm_time){
		AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		if(enabled)
		{
			alarmManager.set(AlarmManager.BOOT,  alarm_time, getPendingIntent(context,ACTION_SCHEDULE_POWER_ON,id));
		}else{
			alarmManager.cancel(getPendingIntent(context,ACTION_SCHEDULE_POWER_ON,id));
		}
	}
	
    	private final static String ACTION_SCHEDULE_POWER_OFF = "android.fireflyapi.action.run_power_off";
	/***
	 * 设置定时关机,id由用户定义，用于开启和关闭定时关机时使用，重启后失效需要重新设置
	 * @param id　　　　　　　　　
	 * @param enabled　　　　开启/关闭
	 * @param alarm_time　开机时间(UTC时间)
	 */
	public static void setSchedulePowerOff(Context context,int id,boolean enabled,long alarm_time){
		AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		if(enabled)
		{
			alarmManager.set(AlarmManager.RTC_WAKEUP,  alarm_time, getPendingIntent(context,ACTION_SCHEDULE_POWER_OFF,id));
		}else{
			alarmManager.cancel(getPendingIntent(context,ACTION_SCHEDULE_POWER_OFF,id));
		}
	}	
	private static PendingIntent getPendingIntent(Context context,String action, int requestCode)
	{
		Intent intent = new Intent(action);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent,PendingIntent.FLAG_UPDATE_CURRENT);
		return pendingIntent;

	}
}