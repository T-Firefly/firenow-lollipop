package android.firefly.util;

import java.io.File;
import android.content.Context;
import android.content.Intent;

public class GpioUtils {

	/**
	 * @param gpioStr
	 * GPIO0_A5，即GPIOX_YN
	 * X取值0-8,一位差32
	 * Y取值A_D,一位差8
	 * N数字0-7
	 * 
	 * @return error -1
	 */
	public  static int gpioParse(String gpioStr)
	{
		if(gpioStr == null || gpioStr.length() != 8){
			System.out.println("input gpio error!");
			return -1;
		};
		gpioStr = gpioStr.toUpperCase();

		if(gpioStr.charAt(4) < '0' || gpioStr.charAt(4) > '8'){
			return -1;
		}
		if(gpioStr.charAt(6) < 'A' || gpioStr.charAt(6) > 'D'){
			return -1;
		}
		if(gpioStr.charAt(7) < '0' || gpioStr.charAt(7) > '7'){
			return -1;
		}
		return + (gpioStr.charAt(4) - '0')*32 + (gpioStr.charAt(6) - 'A')*8 + (gpioStr.charAt(7) - '0') ;
	}
	/**
	 * @param isOccupied 
	 * @param gpio
	 * @param direction in, out, high, low high/low
	 * @param value 1/0
	 * @return  gpioCtrlCmd
	 */
	public static String buildGpioCtrlCmd(int gpio, String direction, int value) {
	    return  "GPIO="+gpio + "\n" 
	                    + "cat /sys/kernel/debug/gpio | grep gpio-$GPIO" + "\n" 
	                    + "if [ $? -ne 0 ] ;then" + "\n"
	                    + "    echo \"grep return a none-zero value, not find gpio-$GPIO\"" + "\n"
	                    + "    echo $GPIO > /sys/class/gpio/export" + "\n"
	                    + "    echo "+ direction +" > /sys/class/gpio/gpio$GPIO/direction" + "\n"
	                    + "fi" + "\n"
	                    + "echo  "+value +" > /sys/class/gpio/gpio$GPIO/value" + "\n";
	}
	
	/***
	 * 
	 * @param gpio
	 * @return
	 */
	public static String buildGpioRemoveCmd(int gpio) {
	    String unexportPath = "echo " + gpio + " > /sys/class/gpio/unexport";
	    return unexportPath;
	}
	
	/***
	 * 读取gpio的值
	 * @param gpio
	 * @return
	 */
	public static String buildGpioReadCmd(int gpio){
		String valuePath;
		String exportPath;
		valuePath ="cat  /sys/class/gpio/gpio" + gpio + "/value";		
		return valuePath;
	}
	/***
	 * 检查gpio口是否被系统占用
	 * @param gpio
	 * @return
	 */
	public  static String checkGpioOccupieCmd(int gpio){
		return "cat /sys/kernel/debug/gpio | grep gpio-"+gpio;
	}



}