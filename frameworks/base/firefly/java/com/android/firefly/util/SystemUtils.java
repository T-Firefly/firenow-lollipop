package android.firefly.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SystemUtils {

	/**
	 * 设置系统时间
	 * @param year
	 * @param month
	 * @param day
	 * @param hour 0-23
	 * @param minute
	 */
	public  static  String buildSetTimeCmd ( int year, int month, int day, int hour, int minute,int second )
	{
		Calendar c = Calendar.getInstance();
		c.set(year, month, day);
		c.set(Calendar.MINUTE, minute);
		c.set(Calendar.SECOND, minute);
		
		SimpleDateFormat formatter = new SimpleDateFormat("MMddHHmmyy");
		String dateString = formatter.format(new Date(year, month, day, hour, minute, second));
		String buildCmd = "date '"+dateString+"'";
		return buildCmd;
	}

	/***
	 * 设置屏幕亮度 0-255
	 * @param brightness
	 */
	public static String buildSetBrightnessCmd(int brightness){
		return "settings put system screen_brightness "+brightness;
	}	

	/**
	 * 获取屏幕亮度 0-255
	 * @return 获取失败为-1
	 */
	public static String buildGetBrightnessCmd(){
		return "settings get system screen_brightness";
	}


}