package android.firefly.util;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.regex.Pattern;

import android.net.IpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.net.LinkAddress;
import android.net.StaticIpConfiguration;
import android.text.TextUtils;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.regex.Pattern;
import android.util.Log;

public class NetworkUtils{


    public static IpConfiguration getStaticIpConfiguration(String mIpaddr, String mMask, String mGw, String mDns1, String mDns2) {

        StaticIpConfiguration mStaticIpConfiguration =new StaticIpConfiguration();
        /*
         * get ip address, netmask,dns ,gw etc.
         */  
        Inet4Address inetAddr = getIPv4Address(mIpaddr);
        int prefixLength = maskStr2InetMask(mMask); 
        InetAddress gatewayAddr =getIPv4Address(mGw); 
        InetAddress dnsAddr = getIPv4Address(mDns1);

        if (inetAddr.getAddress().toString().isEmpty() || prefixLength ==0 || gatewayAddr.toString().isEmpty()
                || dnsAddr.toString().isEmpty()) {
            return null;
        }

        String dnsStr2=mDns2;  
        mStaticIpConfiguration.ipAddress = new LinkAddress(inetAddr, prefixLength);
        mStaticIpConfiguration.gateway=gatewayAddr;
        mStaticIpConfiguration.dnsServers.add(dnsAddr);

        if (!dnsStr2.isEmpty()) {
            mStaticIpConfiguration.dnsServers.add(getIPv4Address(dnsStr2));
        } 

        return new IpConfiguration(IpAssignment.STATIC, ProxySettings.NONE,mStaticIpConfiguration,null);  
    }

    public static Inet4Address getIPv4Address(String text) {
        try {
            return (Inet4Address) android.net.NetworkUtils.numericToInetAddress(text);
        } catch (Exception e) {
            return null;
        }
    }

    /*
     * convert subMask string to prefix length
     */
    public static int maskStr2InetMask(String maskStr) {
        StringBuffer sb ;
        String str;
        int inetmask = 0; 
        int count = 0;
        /*
         * check the subMask format
         */
        Pattern pattern = Pattern.compile("(^((\\d|[01]?\\d\\d|2[0-4]\\d|25[0-5])\\.){3}(\\d|[01]?\\d\\d|2[0-4]\\d|25[0-5])$)|^(\\d|[1-2]\\d|3[0-2])$");
        if (pattern.matcher(maskStr).matches() == false) {
            return 0;
        }

        String[] ipSegment = maskStr.split("\\.");
        for(int n =0; n<ipSegment.length;n++) {
            sb = new StringBuffer(Integer.toBinaryString(Integer.parseInt(ipSegment[n])));
            str = sb.reverse().toString();
            count=0;
            for(int i=0; i<str.length();i++) {
                i=str.indexOf("1",i);
                if(i==-1)  
                    break;
                count++;
            }
            inetmask+=count;
        }
        return inetmask;
    }


    //将子网掩码转换成ip子网掩码形式，比如输入32输出为255.255.255.255  
    public static String interMask2String(int prefixLength) {
        String netMask = null;
        int inetMask = prefixLength;

        int part = inetMask / 8;
        int remainder = inetMask % 8;
        int sum = 0;

        for (int i = 8; i > 8 - remainder; i--) {
            sum = sum + (int) Math.pow(2, i - 1);
        }

        if (part == 0) {
            netMask = sum + ".0.0.0";
        } else if (part == 1) {
            netMask = "255." + sum + ".0.0";
        } else if (part == 2) {
            netMask = "255.255." + sum + ".0";
        } else if (part == 3) {
            netMask = "255.255.255." + sum;
        } else if (part == 4) {
            netMask = "255.255.255.255";
        }

        return netMask;
    }



    /*
     * 返回 指定的 String 是否是 有效的 IP 地址.
     */
    public static boolean isValidIpAddress(String value) {
        int start = 0;
        int end = value.indexOf('.');
        int numBlocks = 0;

        while (start < value.length()) {

            if (-1 == end) {
                end = value.length();
            }

            try {
                int block = Integer.parseInt(value.substring(start, end));
                if ((block > 255) || (block < 0)) {
                    Log.w("EthernetIP",
                            "isValidIpAddress() : invalid 'block', block = "
                                    + block);
                    return false;
                }
            } catch (NumberFormatException e) {
                Log.w("EthernetIP", "isValidIpAddress() : e = " + e);
                return false;
            }

            numBlocks++;

            start = end + 1;
            end = value.indexOf('.', start);
        }
        return numBlocks == 4;
    }


}
