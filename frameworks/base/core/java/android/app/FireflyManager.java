package android.app;

import android.util.Log;
import android.os.IFireflyManager;
import android.content.Context;
import android.os.RemoteException;
import android.os.Build;
import android.os.Environment;
import android.os.storage.StorageManager;

import android.firefly.util.PowerOnOffUtils;
import android.firefly.util.SystemInfoUtils;
import android.firefly.util.SystemUtils;
import android.firefly.util.FileUtils;
import android.firefly.util.SchedulePowerOnOffUtils;
import android.firefly.util.GpioUtils;

import android.view.WindowManager;
import android.util.DisplayMetrics;



public class FireflyManager {
        private static String TAG = "FireflyManager";

        public static final String  API_FEATURE_SWITCH_MIC= "fireflyapi.hardware.switch_mic";

        private static final String VERSION = "20181101";

        IFireflyManager mFireflyManager;
        private Context context;
        public FireflyManager(Context ctx,IFireflyManager fireflyManager) {
            mFireflyManager = fireflyManager;
            context = ctx;
        }

        public  static String getFireflyApiVersion(){
            return VERSION;
        }

    /**  
        * 系统相关信息获取 
        */
        /**
        * 获取目前设备的型号
        */
        public  static String getAndroidModel(){
            return Build.MODEL;
        }

    /**
     * 获取目前设备的android系统的版本
     */
    public  static String getAndroidVersion(){
        return Build.VERSION.RELEASE;
    }

    /***
     * 获取设备的RAM大小,单位为MB
     */ 
    public  static long getRamSpace (){
        return SystemInfoUtils.getRamSpace();

    }

    /***
     * 获取设备的RAM大小，并格式化为String格式
     */ 
    public  static String getFormattedRamSpace(Context context){
        return SystemInfoUtils.getFormattedRamSpace(context);
    }

    /***
     * 获取设备的内置Flash大小,单位为MB
     */
    public  static long getFlashSpace(){
        return SystemInfoUtils.getFlashSpace();
    }
    /***
     * 获取设备的内置Flash大小,并格式化为String格式
     */
    public   static String getFormattedFlashSpace(Context context){
        return SystemInfoUtils.getFormattedFlashSpace(context);
    }

    /***
     * 获取设备的固件SDK版本
     * 有疑问，待确认
     */
    // public  static String getFirmwareVersion(){
    //     return null;
    // }

    /***
     * 获取设备的固件内核版本。
     */
    public  static String getFormattedKernelVersion(){
        return SystemInfoUtils.getFormattedKernelVersion();
    }
    /***
     * 获取设备的固件系统版本和编译日期。
     */ 
    public static String getAndroidDisplay (){
        return Build.DISPLAY;
    }


     /**
     * 获取显示屏分辨率宽X像素
     * @return
     */
    public  static int getScreenWidth(Context context){
        WindowManager wm = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getRealMetrics(metrics);

        return metrics.widthPixels;
    }
    /**
     * 获取显示屏分辨率宽Y像素
     * @return
     */
    public  static int getScreenHeight(Context context){
        WindowManager wm = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));
        DisplayMetrics metrics = new DisplayMetrics();
        wm.getDefaultDisplay().getRealMetrics(metrics);

        return metrics.heightPixels;
    }

   /*******
     * 
     * 开关机
     * 
     */
    /**
     * 系统关机
     * @param confirm is true 弹出是否关机的确认窗口 
     * @hide
     */
    public void shutdown(boolean confirm)
    {
        try {
            mFireflyManager.shutdown(confirm);
        } catch (RemoteException e) {
            
        }
    }

    /**
     * 系统重启
     * @param confirm is true 弹出是否关机的确认窗口 
     * @param reason  is recovery or null
     * @hide
     */
    public void reboot(boolean confirm,String reason)
    {
        try {
            mFireflyManager.reboot(confirm,reason);
        } catch (RemoteException e) {
            
        }
    }

    /**
     * 系统休眠
     *  @hide
     */
    public void sleep()
    {
        try {
            mFireflyManager.sleep();
        } catch (RemoteException e) {
            
        }
    }


    /*******
     * 
     * 系统设置
     * 
     */
    /**
     * [takeScreenshot description]
     * @param  path [description]
     * @param  name [description]
     *  @hide
     */
    public  void  takeScreenshot(String path,String name)
    {
        try {
            mFireflyManager.takeScreenshot(path,name);
        } catch (RemoteException e) {
            
        }
    }

    /**
     * [setRotation description]
     * @param rotation 
     * @hide
     */
    public  void setRotation (int  rotation)
    {
        try {
            mFireflyManager.setRotation(rotation);
        } catch (RemoteException e) {
            
        }
    }

    /**
     * [getRotation description]
     * @return [description]
     * @hide
     */
    public  int getRotation ( )
    {
        try {
            return mFireflyManager.getRotation();
        } catch (RemoteException e) {
            
        }
        return 0;
    }

    /**
     * [thawRotation description]
     * @hide
     */
    public  void thawRotation ( )
    {
        try {
            mFireflyManager.thawRotation();
        } catch (RemoteException e) {
            
        }
    }

    /**
     * [setStatusBar description]
     * @param show [description]
     * @hide
     */
    public void setStatusBar(boolean show)
    {
        try {
            mFireflyManager.setStatusBar(show);
        } catch (RemoteException e) {
            
        }
    }


    public final static int TYPE_DEFAULT_MIC = 0;
    public final static int TYPE_HEADSET_MIC = 1;
    /**
     * MIC切换为板上自带的或者耳麦上。
     * 内核提供录音选择麦克风或耳机录音节点 
     * @param mic_type TYPE_DEFAULT_MIC/TYPE_HEADSET_MIC
     * @return
     * @hide
     */
    public  boolean switchMic(int mic_type){

         try {
            return mFireflyManager.switchMic(mic_type);
        } catch (RemoteException e) {
            
        }
        return false;
    }


    public final static int TYPE_USBHOST = 0;
    public final static int TYPE_OTG = 1;
    /**
     * 设置USB接口电源,选择控制OTG或USB电源接口
     *  echo 1 > /sys/class/otg_hw_ctl/otg_hw_ctl  连接OTG
     *  echo 0 > /sys/class/otg_hw_ctl/otg_hw_ctl  断开OTG
     *  echo 1 > /sys/class/host_hw_ctl/host_hw_ctl  连接USB HOST
     *  echo 0 > /sys/class/host_hw_ctl/host_hw_ctl  断开USB HOST
     * @param type TYPE_USBHOST,TYPE_OTG
     * @param connect 
     * @return
     * @hide
     */
    public  boolean setUsbPower(int type, boolean connect ){
        try {
            return mFireflyManager.setUsbPower(type,connect);
        } catch (RemoteException e) {
            
        }
        return false;
    }


    /**
     * 打开/关闭看门狗
     * @param enable
     * @hide
     */
    public  boolean watchDogEnable(boolean enable){
        try {
            return mFireflyManager.watchDogEnable(enable);
        } catch (RemoteException e) {
            
        }
         return false;
    }
    /**
     * 喂狗一次，对看门狗计数进行复位操作
     * @hide
     */
    public  boolean watchDogFeed(){
        try {
            return mFireflyManager.watchDogFeed();
        } catch (RemoteException e) {
            
        }
         return false;
    }
}